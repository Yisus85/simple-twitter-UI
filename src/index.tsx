import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import App from './components/AppComponent/App';
import './index.css';

ReactDOM.render(
  <div>
      <Router>
        <App />
      </Router>
  </div>
  ,
  document.getElementById('root') as HTMLElement
);
