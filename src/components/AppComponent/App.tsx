import * as React from 'react';
import '../../index.css';
import NavBar from '../NavBar/navBarComponent';

class App extends React.Component {
  public render() {
    return (
      <div className="App">
        <NavBar/>
      </div>
    );
  }
}

export default App;
