export interface IMenuOption {
  exact: boolean;
  glyph: string;
  label: string;
  path: string;
};

export const HomeOption: IMenuOption = {
  exact: true,
  glyph: 'home',
  label: 'Home',
  path: '/',
};

export const SearchOption: IMenuOption = {
  exact: true,
  glyph: 'search',
  label: 'Search',
  path: '/search',
};

export const NotificationsOption: IMenuOption = {
  exact: true,
  glyph: 'refresh',
  label: 'Notifications',
  path: '/notifications',
};

export const MessagesOption: IMenuOption = {
  exact: true,
  glyph: 'envelope',
  label: 'Messages',
  path: '/messages',
};
