import * as React from 'react';
import { Component } from 'react';
import { Button, Glyphicon, Modal } from 'react-bootstrap';
import { NavLink, Redirect, Route, RouteComponentProps, Switch, withRouter } from 'react-router-dom';
import userImgSmall from '../../DanySmall.jpg';
import HomeComponent from '../HomeComponent/HomeComponent';
import MessagesComponent from '../MessagesComponent/MessagesComponent';
import NotificationsComponent from '../NotificationsComponent/NotificationsComponent';
import SearchComponent from '../SearchComponent/SearchComponent';
import { HomeOption, IMenuOption, MessagesOption, NotificationsOption, SearchOption } from './navOptions';

type AppProps = RouteComponentProps<{}> & IAppOwnProps;

// tslint:disable-next-line:no-empty-interface
interface IAppOwnProps { }

export interface IAppComponentState {
  show: boolean;
  tweets: string[];
  twwetText: string;
  loggedUser: string;
}

class NavBarComponent extends Component<AppProps, IAppComponentState>{

  private menuOptions = {
    user: [
      HomeOption,
      MessagesOption,
      NotificationsOption,
      SearchOption
    ]
  }
  constructor(props: AppProps) {
    super(props);
    this.state = {
      loggedUser: "Daenerys Targaryen",
      show: false,
      tweets: [] as string[],
      twwetText: "",
    };

    this.toggleModal = this.toggleModal.bind(this);
    this.setTweet = this.setTweet.bind(this);
    this.setInputValue = this.setInputValue.bind(this);
  }

  public render() {
    const menuOpt: IMenuOption[] = this.menuOptions.user;
    const menuOption: JSX.Element[] = menuOpt.map(this.renderMenuOption);
    const buttonMessage: string = this.props.location.pathname === "/messages" ?
      "envelope" : "pencil";
    const modalTitle: string = this.props.location.pathname === "/messages" ?
      "Type a Message!" : "What's happening?";

    return (
      <div>
        <header className="header">
          <img src={userImgSmall} className="userImage" />
          <h1 className="userName">{this.state.loggedUser}</h1>
        </header>
        <nav>
          <ul className="navBar"> {menuOption} </ul>
        </nav>
        <Switch>
          <Route path="/search" component={SearchComponent} />
          <Route path="/notifications" component={NotificationsComponent} />
          <Route path="/messages" component={MessagesComponent} />
          <Route path="/" render={
              // tslint:disable-next-line:jsx-no-lambda
              (props) => {
                return (
                  <div>
                    <HomeComponent parentState={this.state} userImage={userImgSmall}/>
                  </div>
                );
              }
            }
          />
          <Redirect path="*" to="/" />
        </Switch>

        <button className="tweetButton" onClick={this.toggleModal}>
          <Glyphicon glyph={buttonMessage} />
        </button>

        <Modal show={this.state.show} onHide={this.toggleModal} container={this} aria-labelledby="contained-modal-title">
          <Modal.Header>
            <Modal.Title>{modalTitle}</Modal.Title>
          </Modal.Header>
          <Modal.Body className="row text-center">
            <textarea cols={40} rows={4} id="tweet-text" onChange={this.setInputValue} value={this.state.twwetText}/>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.toggleModal}>
              Close
            </Button>
            <Button bsStyle="primary" onClick={this.setTweet}>
              Save changes
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }

  private setInputValue(e: React.ChangeEvent<HTMLTextAreaElement>) {
    this.setState({
      ...this.state,
      twwetText: e.target.value
    });
  }

  private setTweet(){
    const newArray: string[] = this.state.tweets;
    newArray.unshift(this.state.twwetText);

    this.setState({
      ...this.state,
      tweets: newArray,
      twwetText: ''
    }, () => {
      this.toggleModal();
    });
  }

  private toggleModal() {
    const show: boolean = this.state.show === true ? false : true;
    this.setState({
      ...this.state,
      show
    });
  }

  private renderMenuOption = ({ label, glyph, path, exact }: IMenuOption, idx: number) => {
    const { pathname } = this.props.location;
    const active = (pathname === path) ? 'active' : '';

    return (
      <li className="nav-item" key={idx}>
        <NavLink className={`${active}`} to={path} exact={exact}>
          <Glyphicon glyph={glyph} />
        </NavLink>
      </li>
    );
  }
}

export default withRouter(NavBarComponent);
