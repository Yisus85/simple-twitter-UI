import * as React from 'react';
import { Component } from 'react';
import { Col, Glyphicon, Grid, Row } from "react-bootstrap";
import { IAppComponentState } from "../NavBar/navBarComponent";

interface IHomeComponentOwnProps {
  parentState: IAppComponentState,
  userImage: string;
}

// tslint:disable-next-line:no-empty-interface
export interface IHomeComponentState {
}
export default class HomeComponent extends Component<IHomeComponentOwnProps, IHomeComponentState> {

  constructor(props: IHomeComponentOwnProps) {
    super(props);
    this.renderTweets = this.renderTweets.bind(this);
  }

  public render() {
    const {tweets} = this.props.parentState;
    
    if (tweets.length === 0) {
      return <div />
    }
    return (
      <div>
        {this.renderTweets(tweets)}
      </div>
    );
  }

  private renderTweets(tweets: string[]) {
    const { loggedUser: user}= this.props.parentState;
    return tweets.map(
      (tweet, index) => {
        return (
          <div key={index} className="tweetComponent">
            <Grid>
              <Row>
                <Col xs={2}>
                  <img src={this.props.userImage} className="tweetImage" />
                </Col>
                <Col xs={10}>
                  <article className="tweetBody">
                    <strong>{user}</strong>
                    <br/>
                    {tweet}
                  </article>
                  <footer className="tweetFooter">
                    <Row>
                      <Col xs={3} className="text-center">
                        <Glyphicon glyph="comment" />
                      </Col>
                      <Col xs={3} className="text-center">
                        <Glyphicon glyph="retweet" />
                      </Col>
                      <Col xs={3} className="text-center">
                        <Glyphicon glyph="heart-empty" />
                      </Col>
                      <Col xs={3} className="text-center">
                        <Glyphicon glyph="share" />
                      </Col>
                    </Row>
                  </footer>
                </Col>
              </Row>
            </Grid>
          </div>
        );
      }
    );
  }
}